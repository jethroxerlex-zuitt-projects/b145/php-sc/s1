<?php

/*
 	
 [SECTION] Comments
 Comments are parts of the code that get ignored by the language

 Comments are meant to describe the written code.	

 [SECTION] Variables
 Variables are used to contain data
 Variables are a named location for the stored value.
 Variables are defined in PHP using the dollar ($) notation before the name of the variable	

*/

 $name = "John Smith";
 $email = "johnsmith@gmail.com";

 $name = "John Johnson";


 /*
  [SECTION] Constants
  Constants used to hold data that are meant to be read-only(value cannot be changed)
  Constants are defined using the define() function	
  
 */

  define("PI", 3.1416);

 /*
 	define(var_name, var_value)

 	By convention, constant names are in ALL CAPS
 */ 

/*
	[SECTION] Data Types

	Strings:

*/ 

$state = 'New York';
$country = 'United States of America';
$address = "$state, $country";
// $address = $state . ", ". $country; 	


// Integers
$age = 31;
$headcount = 26;

//Floats
$grade = 98.2;
$distanceInKilometers = 1342.12;

//Boolean
$hasTraveledAbroad = false;
$hasSymptoms = true;

//Null
$spouse = null;

//Objects

$person = (object)[

	'fullName' => 'John Smith',
	'isMarried' => false,
	'age' => 35,
	'address' => (object)[
		'state' => 'New York',
		'country' => 'United States of America'

	]
];

//Arrays

$grades = array(98.7, 92.1, 90.2, 94.6);



/*
	[SECTION] Operators

	Assignment Operator
	 -Used to assign values to variables


*/

$x = 1234;
$y = 1235;	 


$isLegalAge = true;
$isRegistered = false;


/*
 [SECTION] Functions
 Functions are used to make reusable code that can be called/invoked
*/

function getFullName($firstName,$middleInitial,$lastName){
    return "$lastName, $firstName $middleInitial";
} 

/*
 [SECTION] Selection Control Structures/Conditional Statements

 Selection Control Structures are used to make code execution dynamic according to predefined conditions.   

*/

 function getTyphoonIntensity($windSpeed){
    if ($windSpeed <30) {
        return "Not a typhoon yet";
    }else if ($windSpeed <=61) {
       return 'Tropical depression detected';     
      }else if ($windSpeed >62 && $windSpeed <= 88) {
        return 'Tropical storm detected';
    }else if ($windSpeed >= 89 && $windSpeed <= 117) {
        return 'Severe tropical storm detected';
    }else{
        return 'Typhoon detected';
    }
 }

 //If-else statements do not need to be wrapped in a function


 //Ternary Operator

 function isUnderAge($age) {
    return ($age < 18) ? true : false;
 }

 //(condition) ? statement1 : statement2;
 //if condition is true, execute statement 1. If not, execute statement 2.
 //Also does not need to be in a function